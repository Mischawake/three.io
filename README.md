## Install

```console
$ npm install
```

## Running

```console
$ npm start
```

Visit http://localhost:8080

## SSH
you can view the log output by doing
sudo journalctl -f -u io

you can also stop the app
sudo systemctl stop io
and then run it yourself
and then sudo systemctl start io 
to run the daemon again

deploy/deploy.sh XXX.XX.XXX.XXX 
ssh -i ~/Developer/three.io/deploy/auth/machine -p 5022 mrio@XXX.XX.XXX.XX

## Hot reloading
Changing any *.js file will automatically refresh the webpage.