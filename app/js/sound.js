class SoundManager{

    constructor(){

        window.createjs.Sound.registerSound("../assets/Button.wav", "button" );

    }

    PlayButton(){
        let instance = window.createjs.Sound.play( "button" );
    }

}

const soundManager = new SoundManager();

module.exports = {
    soundManager
};