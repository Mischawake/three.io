const _ = require('lodash');
const _$ = require('jquery');
const now = require('performance-now');

//THREE.JS
const THREE = require('three')
  , EffectComposer = require('three-effectcomposer')(THREE);

//Mr Doob Stats
const Stats = require('stats.js');

//My Engine Classes
const Shaders = require('./js/shaders');
const GeoBarn = require('./js/geo').GeoBarn; //weird naming workaround
const Seg16 = require('./js/geo').Seg16;
const Helpers = require('./js/helpers').Helpers;

//Globals
const DebugHud = require('./js/debug').debugHud;
const Input = require('./js/input').input;
const Key = require('./js/input').Key; //part of input
const SoundManager = require('./js/sound').soundManager;

//Consts
const FRUSTUM = 30;

let stats = new Stats(); // mrdoob stats
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom


class Game {
    constructor(sendFn) {
        
        this.send = sendFn;
        this.playerId = 0;
        
        this.timeSinceSend = 0;
        this.elapsedTime = 0;

        // //THREE JS
        this.renderer = new THREE.WebGLRenderer({
            antialias: true
        });

        this.canvas = this.renderer.domElement;//document.getElementById('canvas');

        let container = document.getElementById('webgl-container');
        container.appendChild(this.canvas);
        container.appendChild(stats.dom); // here because must be done after dom loads

        //this.canvas.style.cursor = "none";
        this.ctx = this.canvas.getContext('2d');
        this.scene = new THREE.Scene();

        // // Perspective Camera
        // //camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 3500);

        // ortho cam
        let aspect = window.innerWidth / window.innerHeight;
        this.cameraFrustum = FRUSTUM;
        this.camera = new THREE.OrthographicCamera( this.cameraFrustum * aspect / - 2, this.cameraFrustum * aspect / 2, this.cameraFrustum / 2, this.cameraFrustum / - 2, 1, 1000 );

        this.camera.rotation.x = -Math.PI / 2.0; //looking down
        this.camera.rotation.y = 0;
        this.camera.rotation.z = 0;

        //this.cameraLook = new THREE.Vector3(0, 0, 0);
        //this.cameraLookTarget = new THREE.Vector3(-3, 0, -3);
        
        this.scene.add(this.camera);
        
        this.renderer.setClearColor( new THREE.Color( 0x111111 ).convertGammaToLinear() );
        this.renderer.setPixelRatio(window.devicePixelRatio); //2 on retina
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.shadowMap.enabled = false;

        this.composer = new EffectComposer(this.renderer);
        let renderPass = new EffectComposer.RenderPass(this.scene, this.camera);
        this.bloomPass = new Shaders.HyperBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight));//1.0, 9, 0.5, 512);
        let copyShader = new EffectComposer.ShaderPass(EffectComposer.CopyShader);

        copyShader.renderToScreen = true;

        this.composer.addPass(renderPass);  
        this.composer.addPass(this.bloomPass);
        this.composer.addPass(copyShader);

        this.geoBarn = new GeoBarn( this.scene );
        this.seg16 = new Seg16( this.geoBarn );

        window.addEventListener('resize', e => this.resize(), false );
        this.resize();

        //GAME SPECIFIC VARS

    }

    update(dt, time) {
        stats.begin();

        this.timeSinceSend += dt;
        this.elapsedTime += dt;
       
        //networking pulse
        if( this.timeSinceSend > 1.0 ){
            
            this.timeSinceSend = 0.0;
            this.broadcast();
          
        }

        //interactions
        let interact = Input.keyPressed(Key.Mouse) || Input.keyPressed(Key.Space);
        if( interact ) SoundManager.PlayButton();

        //Cover Art
        //this.geoBarn.drawFilledCircle( this.circle, 0.25, new THREE.Color( 'white' ) );
        //this.geoBarn.drawCircle( this.circle, 0.5, new THREE.Color( 'cyan' ) );
        //this.geoBarn.drawCircle( new THREE.Vector3(0,-1,0), 5.0, new THREE.Color( 'cyan' ), 36 );
        //this.seg16.writeWord( "You are the hero in this dream", new THREE.Vector3( 0, 0, 0 ), 0.34, new THREE.Color('cyan'), true, true, 0.5 ); 
        //DebugHud.addLine("This is a test");
        this.camera.position.set( 0, 20, 0);

        this.geoBarn.drawCircle( new THREE.Vector3(0), 5, new THREE.Color( 'cyan' ), 100.0 );
       
        //do not modify below here...
        this.render();
        Input.flush();
        stats.end();
    }

    broadcast(){

        // let arrayBuffer = new ArrayBuffer( 1 ); 
        // let dataView = new DataView(arrayBuffer);
        // let offset = 0;
        // dataView.setUint8(offset, Math.random() > 0.5 ? 1 : 0 ); 

        // this.send('binary', arrayBuffer );
        // this.send('text', 'hello world');

    }

    render(){

        this.geoBarn.flush();
        DebugHud.flush();

        this.renderer.toneMapping = THREE.Uncharted2ToneMapping;
        this.renderer.toneMappingExposure = 1.0; //adjust how bright the brights are
        //this.renderer.render( this.scene, this.camera );
        this.composer.render();

    }

    deserialize( data ){

        let dataView = new DataView(data); 

        let output = "";
        for( let i = 0; i < 8; i++ ){
            output += dataView.getUint8( i );
        }

        console.log( output );
         
        // let arrayBuffer = null;
        // if (typeof(data) === "object") {
        //     if (data instanceof ArrayBuffer) { arrayBuffer = data; }
        //     if (data instanceof Buffer) {
        //         arrayBuffer = new ArrayBuffer(data.length);
        //         let view = new Uint8Array(arrayBuffer);
        //         for (let i = 0; i < data.length; i++) {
        //             view[i] = data[i];
        //         }
        //     }
        // }

        // if (arrayBuffer && arrayBuffer.byteLength > 0) {
        //     let dataView = new DataView(arrayBuffer);
        //     let offset = 0;
        //     console.log( "Deserialize: "+ arrayBuffer.byteLength + " bytes" );
        // }

    }

    resize(){

        let aspect = window.innerWidth / window.innerHeight;

        let targetArea = this.cameraFrustum * this.cameraFrustum;

        this.camera.left   = - this.cameraFrustum / 2 * aspect;
        this.camera.right  =   this.cameraFrustum / 2 * aspect;
        this.camera.top    =   this.cameraFrustum / 2;
        this.camera.bottom = - this.cameraFrustum / 2; 

        // // keep area consistent:
        let w = this.camera.right * 2;
        let h = this.camera.top * 2;
        let area = w * h;
        let zoom = Math.sqrt( targetArea / area );

        zoom = Helpers.clamp( zoom, 0, 2);

        this.camera.left *= zoom;
        this.camera.right *= zoom;
        this.camera.top *= zoom;
        this.camera.bottom *= zoom;

        this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.composer.setSize( window.innerWidth, window.innerHeight); //<! breaks stuff?
        
        this.camera.updateProjectionMatrix();

    }
}

//
// App
//
const AppMode = {
    None: 0,
    Title: 1,
    Waiting: 2,
    Playing: 3,
    LostConn: 4
};


class App {
    constructor() {

        this.game = new Game((t, d) => {
            this.sendMessage(t, d);
        });

        _$('#startBtn').click(() => {
            this.setMode(AppMode.Waiting);
            this.connect();
        });

        this.mode = AppMode.None;
        this.setMode(AppMode.Title);

        this.prevTime = 0;
        this.update();
        
    }

    connect() {
        var host = window.document.location.host;
        this.ws = new WebSocket('ws://' + host + '/play');

        this.ws.binaryType = "arraybuffer";

        this.ws.onerror = (error) => {
            console.log('socket error', error);
        };

        this.ws.onopen = () => {
            console.log('Sending join');
            this.sendMessage('ready', {});
            
            this.setMode(AppMode.Playing);
        };

        this.ws.onmessage = (e) => {

            let decode = function(data) {
                if (typeof(data) === 'object' ){
                    return {
                        type: 'binary',
                        data: data
                    };
                }
                else if (typeof(data) === 'string') {
                    return JSON.parse(data);
                } else {
                    return {
                        type: 'unknown type: ' + typeof(data),
                        data: data
                    };
                }
            };

            let msg = decode(e.data);
            this.onMessage(msg.type, msg.data);

        };

        this.ws.onclose = () => {
            console.log('socket closed');
            this.setMode(AppMode.LostConn);
        };
    }

    sendMessage(type, data) {

        let msg;
        if( type === "binary" ){
            msg = data;
        }
        else{
            msg = JSON.stringify({
                type: type,
                data: data
            });
        }
        this.ws.send(msg, (err) => {
            if (err !== undefined) {
                console.log('Send error', err);
            }
        });
    }

    onMessage(type, data) {
        switch (type) {
        case 'joined':
            console.log('Connected as player', data.playerId);
            this.game.playerId = data.playerId;
            this.setMode(AppMode.Playing);
            break;
        case 'binary':
            this.game.deserialize( data );
            break;
        default:
            console.log('Unknown message', type, data);
        }
    }

    update() {

        let time = now();
        let dt = (time - this.prevTime) / 1000.0;
        this.prevTime = time;
        requestAnimationFrame((t) => this.update(t));

        this.game.update(dt, time);

    }

    setMode(mode) {

        console.log("Mode: "+mode);
        if (this.mode != mode) {
            this.mode = mode;

            _$('#splashHolder').toggle(mode == AppMode.Title);
 
        }
    }
}


function reverseArray( arr ){

    let newArray = [];

    for( let i = arr.length - 1; i >= 0; i-- ){

        newArray.push( arr[i] );
    }

    return newArray;

}


function run() {

    let app = new App();
    DebugHud.container =  document.getElementById("debugHud");
}

window.addEventListener('load', e => { run(); });
