const _ = require('lodash');
const assert = require('assert');
const now = require('performance-now');

const kTickRate = 60;
const MAX_PLAYERS = 100;

//
// Player
//
class Player {
    constructor(game, ws, id) {
        
        this.id = id;
        this.ws = ws;
        this.heartbeat = now();
        this.ready = false;

        ws.binaryType = "arraybuffer"; 

        ws.on('message', (data) => {
            this.heartbeat = now();

            let recvMsg = (m) => {
                if (typeof(m) === 'object' ){
                    return {
                        type: 'binary',
                        data: data
                    };
                }        
                else if (typeof(m) === 'string') {
                    return JSON.parse(m);
                } else {
                    return {
                        type: 'unknown type: ' + typeof(data),
                        data: data
                    };
                }
            };

            let msg = recvMsg(data);
            game.onMessage(this, msg.type, msg.data);

        });

        ws.on('error', (err) => {
            console.log('Error', err);
        });

        ws.on('close', () => {
            game.removePlayer(this);
            this.ws = null;
        });
    }

    sendMessage(type, data) {

        let msg;

        if( type === "binary" ){
            msg = data;
        }
        else{
            msg = JSON.stringify({
                type: type,
                data: data
            });
        }

        this.ws.send(msg, (err) => {
            if (err !== undefined) {
                console.log('Send error', err);
            }
        });
    }
}

//
// Game
//

class Game {

    constructor(id) {
        this.id = id;
        this.players = [];
        this.timeSinceUpdate = 0;
        this.started = true;
    }

    onConnection(ws) {
        let id = this.players.length;
        let p = new Player(this, ws, id);
        this.players.push(p);
        console.log('Player joined', this.id, p.id);
    }

    onMessage(player, type, data) {
        switch (type) {
        case 'ready':
            console.log('Player ready', this.id, player.id);
            player.ready = true;
            player.sendMessage('joined', {
                playerId: player.id
            });
            break;
        case 'binary':
            this.deserialize( data );
            break;
        default:
            console.log('Unknown message', this.id, player.id, type, data);
        }
    }

    removePlayer(player) {
        console.log('Player left', this.id, player.id);
        this.players.splice(this.players.indexOf(player), 1);
    }

    isFinished() {
        return this.finished;
    }

    broadcast(type, data) {
        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            p.sendMessage(type, data);
        }
    }

    deserialize( data ){

        let dataView = new DataView(data);
    }



    update(dt) {
        
        if (!this.started || this.isFinished() || this.players.length == 0 ) {
            return;
        }

        this.timeSinceUpdate += dt;

        if( this.timeSinceUpdate > 1.0 / 10.0 ){

            this.timeSinceUpdate = 0.0;

            let arrayBuffer = new ArrayBuffer( 8 ); 
            let dataView = new DataView(arrayBuffer);
            let offset = 0;
            for( let i = 0; i < 8; i++ ){
                 
                dataView.setUint8(offset, Math.random() > 0.5 ? 1 : 0 ); 
                offset += 1;
            
            }
          
            //this.broadcast('binary', dataView );

        }
   
    }

}

//
// GameManager
//
class GameManager {
    constructor() {

        this.games = [];
        this.gameId = 0;
        this.prevTime = now();

        setInterval(() => { this.update(); }, 1000.0 / kTickRate);
    }

    onConnection(ws) {
        let game = null;
        if (this.games.length === 0 ||
            this.games[this.games.length - 1].players.length == MAX_PLAYERS) {
            game = new Game(this.gameId++);
            this.games.push(game);
            console.log('Created game', game.id);

        } else {
            game = this.games[this.games.length - 1];
        }

        game.onConnection(ws);
    }

    update() {
        let time = now();
        let dt = Math.min((time - this.prevTime) / 1000.0, 1.0 / 8.0);
        this.prevTime = time;

        for (let i = this.games.length - 1; i >= 0; i--) {
            let g = this.games[i];

            g.update(dt);

            if (g.isFinished()) {
                this.games.splice(i, 1);
            }
        }
    }
}

module.exports = {
    Game,
    GameManager
};
